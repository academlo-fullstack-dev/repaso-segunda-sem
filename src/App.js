import React from 'react';
import './App.css';
import { AiFillAndroid } from "react-icons/ai";

import Botones from './components/botones';

class App extends React.Component {

  constructor(){
    super();
    this.state = {
      semaforo: 'verde'
    }
  }

  cambiarRojo = () => {
    this.setState({semaforo: 'rojo'});
  }

  cambiarAmarillo = () => {
    this.setState({semaforo: 'amarillo'});
  }

  cambiarVerde = () => {
    this.setState({semaforo: 'verde'});
  }

  render(){
    switch(this.state.semaforo){
      case 'verde': 
        return (
          <div className="App">
            <AiFillAndroid />
            <div className="semaforo">
              <div className="led verde"></div>
              <div className="led"></div>
              <div className="led"></div>
            </div>
            <Botones metodos={
                {
                  cambiarRojo: this.cambiarRojo, 
                  cambiarVerde: this.cambiarVerde, 
                  cambiarAmarillo: this.cambiarAmarillo 
                }
              } />
          </div>
        )
        case 'rojo': 
          return (
            <div className="App">
              <AiFillAndroid />
              <div className="semaforo">
                <div className="led"></div>
                <div className="led"></div>
                <div className="led rojo"></div>
              </div>
              <Botones metodos={{cambiarRojo: this.cambiarRojo, cambiarVerde: this.cambiarVerde, cambiarAmarillo: this.cambiarAmarillo}} />
            </div>
          )
        case 'amarillo': 
          return (
            <div className="App">
              <AiFillAndroid />
              <div className="semaforo">
                <div className="led"></div>
                <div className="led amarillo"></div>
                <div className="led"></div>
              </div>
              <Botones metodos={{cambiarRojo: this.cambiarRojo, cambiarVerde: this.cambiarVerde, cambiarAmarillo: this.cambiarAmarillo}} />
            </div>
          )
      default: 
        break;
    }
    
  }
}

export default App;
