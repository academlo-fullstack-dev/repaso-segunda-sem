import React from 'react';

export default function Botones(props){
    return (
        <div>
          <button onClick={props.metodos.cambiarVerde}>Verde</button>
          <button onClick={props.metodos.cambiarAmarillo}>Amarillo</button>
          <button onClick={props.metodos.cambiarRojo}>Rojo</button>
        </div>
    )
}
